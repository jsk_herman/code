import os
os.system('cls')
#------------------------------------------------------------------------------

prompt = "\nWhat topping would you like to add to your pizza?"
prompt += "\nEnter 'quit' if you do not want to add anymore toppings. "
toppings = ""

while toppings != 'quit':
    toppings = input(prompt)
    if toppings == 'quit':
        print("Then we'll serve your pizza in a short while.")
        break

    print(f"Okay! Adding {toppings.title()} to your pizza...")

