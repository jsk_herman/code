import os
os.system('cls')
#------------------------------------------------------------------------------

num = input("Give me a number and I'll tell you if it's a multiple of 10: ")
num = int(num)

if num % 10 == 0:
    print("\nThe number " + str(num) + " is a multiple of 10.")
else:
    print("\nThe number " + str(num) + " is not a multiple of 10.")