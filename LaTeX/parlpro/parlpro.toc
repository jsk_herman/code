\babel@toc {english}{}
\contentsline {chapter}{Note to the Reader}{i}{chapter*.1}%
\contentsline {chapter}{Contents}{ii}{section*.2}%
\contentsline {chapter}{\chapternumberline {1}The Basics}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Structure of an Organization}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Applying Democratic Principles to Organizations}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}Defining Parliamentary Procedure}{4}{section.1.3}%
\contentsline {chapter}{\chapternumberline {2}The Order of a Business Meeting}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Planning and Using Agendas}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Accepted Order of Business}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}Adopting the Agenda}{7}{section.2.3}%
\contentsline {section}{\numberline {2.4}Quorum}{8}{section.2.4}%
\contentsline {section}{\numberline {2.5}What to Do When No Quorum Is Present}{9}{section.2.5}%
\contentsline {chapter}{\chapternumberline {3}Presenting Business to the Assembly}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}Basic Steps in Presenting a Motion}{10}{section.3.1}%
\contentsline {section}{\numberline {3.2}Making a Main Motion}{11}{section.3.2}%
\contentsline {section}{\numberline {3.3}Discussing a Motion}{12}{section.3.3}%
\contentsline {section}{\numberline {3.4}Taking the Vote}{14}{section.3.4}%
\contentsline {section}{\numberline {3.5}Completing the Action on the Motion}{15}{section.3.5}%
\contentsline {section}{\numberline {3.6}Important Points to Remember Before Making a Motion}{15}{section.3.6}%
\contentsline {section}{\numberline {3.7}Resolutions}{16}{section.3.7}%
\contentsline {chapter}{\chapternumberline {4}Debating the Motion}{17}{chapter.4}%
\contentsline {section}{\numberline {4.1}Rules of Debate}{17}{section.4.1}%
\contentsline {section}{\numberline {4.2}Limitations on Debate}{20}{section.4.2}%
\contentsline {section}{\numberline {4.3}Debatable and Undebatable Motions}{20}{section.4.3}%
\contentsline {chapter}{\chapternumberline {5}Voting}{22}{chapter.5}%
\contentsline {section}{\numberline {5.1}Procedure for Taking a Vote}{22}{section.5.1}%
\contentsline {section}{\numberline {5.2}The Majority Rules}{23}{section.5.2}%
\contentsline {subsection}{Majority Vote Defined}{24}{section*.3}%
\contentsline {subsection}{Modifications in Majority Vote}{24}{section*.4}%
\contentsline {subsection}{A Majority of those Present}{25}{section*.5}%
\contentsline {subsection}{A Majority of the Entire Membership}{25}{section*.6}%
\contentsline {subsection}{A Majority of the Fixed Membership}{26}{section*.7}%
\contentsline {section}{\numberline {5.3}A Two-Thirds Vote}{27}{section.5.3}%
\contentsline {section}{\numberline {5.4}A Three-Fourths Vote}{28}{section.5.4}%
\contentsline {section}{\numberline {5.5}The Tie Vote}{28}{section.5.5}%
\contentsline {section}{\numberline {5.6}Doubting the Result of the Vote}{28}{section.5.6}%
\contentsline {subsection}{Calling for a Division}{29}{section*.8}%
\contentsline {subsection}{Doubting the Result of a Ballot Vote or Roll Call Vote}{29}{section*.9}%
\contentsline {chapter}{\chapternumberline {6}\ignorespaces Motions}{33}{chapter.6}%
\contentsline {chapter}{\chapternumberline {7}Meetings}{42}{chapter.7}%
\contentsline {section}{\numberline {7.1}The Basics}{42}{section.7.1}%
\contentsline {section}{\numberline {7.2}Meeting Strategies}{43}{section.7.2}%
\contentsline {subsection}{Strategy to Adopt an Action}{44}{section*.10}%
\contentsline {subsection}{Strategy to Delay an Action}{45}{section*.11}%
\contentsline {subsection}{Strategy to Defeat a Motion}{46}{section*.12}%
\contentsline {subsection}{Compromise as a Strategy}{46}{section*.13}%
\contentsline {subsection}{Other Strategies}{46}{section*.14}%
\contentsline {subsection}{Voting Strategies}{49}{section*.15}%
\contentsline {chapter}{\chapternumberline {8}\ignorespaces Most Frequently Asked Questions}{50}{chapter.8}%
\contentsline {appendix}{\chapternumberline {A}\ignorespaces Correct Parliamentary Terminology}{80}{appendix.A}%
\contentsline {appendix}{\chapternumberline {B}\ignorespaces Meeting Script}{99}{appendix.B}%
\contentsline {appendix}{\chapternumberline {C}Steps in Making a Motion}{104}{appendix.C}%
\contentsline {appendix}{\chapternumberline {D}Ranking of Motions}{106}{appendix.D}%
\contentsline {appendix}{\chapternumberline {E}Motions That Take a Majority Vote}{107}{appendix.E}%
\contentsline {appendix}{\chapternumberline {F}Motions That Take a Two-Thirds Vote}{108}{appendix.F}%
\contentsline {appendix}{\chapternumberline {G}Proposed Agenda and Order of Business}{109}{appendix.G}%
\contentsline {chapter}{Bibliography}{110}{appendix*.16}%
