\chapter{The Basics}

	In a democratic society, we hold dear many principles of conduct and self-government. When people come together in their organizations and governments to conduct business, certain rules, referred to collectively as \emph{parliamentary procedure}, must be applied correctly to maintain these democratic principles.
	
	Organization members commonly make two mistakes: They do not know parliamentary procedure at all, and/or they misapply it because they don’t understand the underlying democratic principles or they want to manipulate them. These mistakes invariably lead to confusion and, in the worst cases, can result in intimidation and the loss of members’ rights. All members and their organizations must understand the fundamental principles behind parliamentary procedures to ensure the preservation of the democratic process.
	
	\section{Structure of an Organization}
	
	There are basically two ways to structure an organization. One way is based on the \emph{authoritarian model}, which favors the concentration of power in a leader or a small group of people who may or may not be responsible to the members. In the extreme form of this model, one person or a small group (such as a board of directors) may make all the decisions with no input or final approval from the membership.
	
	The second way to structure an organization is based on
	the \emph{democratic model}, which means that the people or the members govern. In the extreme form of this model, the members, not elected representatives, make all decisions. However, in most organizations, there is an agreed upon balance of power achieved between members and the officers they elect.
	
	The democratic style of government is founded upon laws and the rights and responsibilities of all the members, not the whims of an unaccountable leadership. Abraham Lincoln defined democratic government as “government of the people, by the people, and for the people.” An organization that has no rules or governing documents to establish a course of action eventually finds itself in a state of anarchy. In the words of Henry M. Robert, who wrote what we know today as \emph{Robert’s Rules of Order}, “Where there is no law, but every man does what is right in his own eyes, there is the least of real liberty.”
	
	\section{Applying Democratic Principles to Organizations}
	
	For an organization to survive and grow, the democratic model has proved to be the best form of government because it makes use of the talents and abilities of all the members. Organizations are democratic to the extent that they conform in the following ways:
	
	\begin{itemize}
		\item \textbf{The members rule through a decision-making process that they’ve established by a vote.} The organization’s governing documents — its constitution, bylaws, rules of order, standing rules, policy statements, and parliamentary authority (such as Robert’s Rules of Order) — embody this process. This is government by the consent of the governed.
		\item \textbf{Ideas come from the members and are presented to the assembly to decide upon.} Everyone gets the right to present, speak to, and vote on ideas.
		\item \textbf{Leaders come from the people through an election process.} When a leader’s term of office ends, he or she returns to the people. A hierarchy of power doesn’t exist; it is shared equally. All members have the right to be considered for office.
		\item \textbf{Checks and balances between the leadership and the members are established in the governing documents.} As an example of checks and balances, officers and boards of directors have only the power that the governing documents assign to them. Those powers not specifically given to officers and boards in the bylaws enable members to reverse decisions made by boards and officers. For example, if the bylaws do not say that the board or officers can set dues, and the board votes do this, then the members can rescind the action. Another check and balance that the bylaws give is the right of the membership to remove ineffective or tyrannical leaders from office.
		\item \textbf{All members are equal—they have equal rights and responsibilities.}
		\item \textbf{The organization is run with impartiality and fairness.} Law and enactment rule the organization, not the whims of the leadership. The rules are applied equally, impartially, and fairly to all and not just a select few.
		\item \textbf{There is equal justice under the law; members and officers have a right to a fair trial if accused.} Written procedures exist for removing and replacing an officer when the officer doesn’t fulfill his or her duties.
		\item \textbf{The majority rules, but the rights of the minority and absent members are protected.}
		\item \textbf{Everything is accomplished in the spirit of openness, not secrecy.} Members have the right to know what is going on within the organization by attending meetings, inspecting the official records, and receiving notices and reports from committees, officers, and boards.
		\item \textbf{Members have the right to resign from office or from the organization.}
	\end{itemize}

	For a democracy to succeed, the members must work harmoniously together. To accomplish this, each member must know the purpose and goals of the organization, its rules, the rights of each individual member, and what each member is expected to do. One of the greatest threats to a democratic organization is for the members to become apathetic and let a small group of the membership do all the work. This creates divisions and promotes authoritarianism. Another threat is for a small group to work secretly behind the scenes to accomplish its own goals or its own agenda and then push it through without the rest of the membership having an input either through discussion or through the investigative process. Such actions cause mistrust and hostility.
	
	If the principles of democracy are not upheld in the organization, knowing and following the rules of parliamentary procedure is valueless.
	
	\section{Defining Parliamentary Procedure}
	
	Parliamentary procedure enables members to take care of business in an efficient manner and to maintain order while business is conducted. It ensures that everyone gets the right to speak and vote. Parliamentary procedure takes up business one item at a time and promotes courtesy, justice, and impartiality. It ensures the rule of the majority while protecting the rights of the minority and absent members. Adhering to parliamentary procedure is democracy in action.