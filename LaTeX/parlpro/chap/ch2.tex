\chapter{The Order of a Business Meeting}

	A business meeting provides members with the opportunity to propose ideas and to participate in forming the plans and actions of the organization. To do this in an orderly and efficient fashion, the business of the meeting is conducted according to the first principle of parliamentary procedure, which states that business is taken up one item at a time. The plan or the established order in which the items of business are taken up is called an \emph{agenda}. This is a Latin word meaning “things to be done.” Common parliamentary law over the years has arrived at an accepted order for a business meeting. Sometimes, however, an organization may wish to follow a different order of business. In that case, the organization must write the order of business in its own rules of order, which should be with, but not part of, the bylaws.
	
	This chapter introduces the accepted order of business and explains how to plan and adopt an agenda, as well as determine when special kinds of agendas are needed. It gives an overview of each aspect of the agenda, from determining a quorum and receiving reports from officers and committees to hearing new business and adjourning the meeting.
	
	\section{Planning and Using Agendas}
	
	In any kind of meeting, the person leading the meeting should preside from an agenda — an outline of items, listed in order of importance, that are to be accomplished at the meeting. Having an agenda keeps the meeting on track and saves time.
	
	The basic structure of an agenda comes from the order of business as established either by the parliamentary authority or by the rules of the organization.
	
	\section{Accepted Order of Business}
	
	This section outlines the commonly used order of the agenda. Before any business can be transacted at a meeting, the president must determine that a \emph{quorum} (the required minimum number of members needed to have a meeting) is present. The president then calls the meeting to order. He or she proceeds with the organization’s established order of business. If an organization has no established order of business, the following is the customary order of business for organizations that have regular meetings within a quarterly time period.
	
	\begin{enumerate}
		\item \textbf{The minutes of the previous meeting are read and approved.} Often members want to dispense with the reading of the minutes because they do not feel that the minutes are important to hear. However, keep in mind that the minutes are a legal document for the organization. By approving the minutes, the members agree that this is what happened at the meeting. When a legal action has been brought against the organization, courts use minutes for evidence. Therefore, it is important that the assembly (or a committee named for the purpose of approving the minutes) approves the minutes. There is no time limit on minute corrections.
		
		The minutes also serve to inform members who were absent from the previous meeting of what happened at the meeting. The minutes provide an opportunity to correct oversights. For example, there may be motions that carry over business to the present meeting that are in the minutes but not on the agenda. Members who are alert while the minutes are being read can ask that these motions be added to the agenda of the present meeting. Another important point is that the motion \emph{lay on the table}, which allows members to temporarily set aside a motion in order to take up more urgent business, is recorded in the minutes but not put on the agenda. It is a parliamentary rule that, because the members vote to lay the motion on the table, only the members can make a motion to take it from the table. By listening carefully when the minutes are read, members take note of this and know the right course of action to take.
		
		\item \textbf{The reports of officers, boards, and standing committees (those listed in the bylaws) are read and discussed.}
		
		\item \textbf{The reports of special committees (if there are any) are heard.}
		
		\item \textbf{Any special orders are presented.} These are motions postponed to the meeting and by a two-thirds vote made a special order so that they come up before unfinished business (see Chapter 6). Or, a \emph{special order} can be special business that comes up once a year, such as nominations and elections.
		
		\item \textbf{Unfinished business and general orders are discussed.} \emph{Unfinished business} is a motion that was under discussion at the time that the previous meeting adjourned. A \emph{general order} is a motion that was postponed to the current meeting but not made a \emph{special order}. (These terms apply only in meetings of groups that meet quarterly or more often.)
		
		\item \textbf{The members proceed to new business.} New business proposes an issue that is new to this meeting. It may be something not discussed before or something that was defeated at a past meeting (or even at the last meeting).
		When the agenda items are finished and the assembly has no further business to propose, it’s time to adjourn.
	\end{enumerate}

	\section{Adopting the Agenda}
	
	Although members may adopt the agenda at the beginning of the meeting, the agenda shouldn’t tie the hands of the assembly, prevent members from bringing up business, or enable a small group to railroad through their pet projects. Agendas should have flexibility to provide for unseen things that may come up in a meeting. Some organizations want to adopt an agenda believing that they can add no further items as the meeting progresses, which is not true. If an agenda is adopted, changing it takes a two-thirds vote.
	
	An organization can adopt an agenda only if its governing documents don’t include rules of order dictating the order of a business meeting. (Rules of order unique to a particular organization are usually included with, but not part of, the bylaws.)
	
	In some types of meetings — those that occur less than quarterly, conventions, or other sessions that may last for several days — adopting the agenda is most important. Because these meetings take place infrequently, adopting an agenda ensures that participants will accomplish the tasks on the agenda without getting sidetracked by other issues. A majority vote adopts an agenda. After it’s adopted, only a two-thirds vote or general consent may change the agenda.
	
	\section{Quorum}
	
	As discussed earlier in the chapter, before an organization can legally transact any business at a meeting, a quorum must be present. Quorum is a Latin word meaning “of them,” as in “do we have enough of them — the members?”
	
	A quorum is the minimum number of members who must be present in order to conduct business. The organization’s bylaws should contain this number. If the bylaws don’t contain a quorum number, then, according to parliamentary law, the quorum is a majority of the entire membership. The presiding officer should know what that number is and make sure that a quorum is present before calling the meeting to order. To establish that a quorum is present, the president can take a head count of those present, the secretary can call the roll, or members can sign in. However, the officer does not have to state that a quorum is present when he or she calls the meeting to order. If, as a member, you’re unsure whether a quorum is present, you may ask the presiding officer after he or she calls the meeting to order.
	
	Never conduct a business meeting without a quorum present. If business is transacted without a quorum, it is null and void. It is also important that a quorum be present throughout the entire time that business transactions take place. If you notice that people have left the meeting and a quorum is no longer present, it is your duty to raise a point of order (which points out a breach of the rules) by informing the presiding officer that a quorum is no longer present and any business transacted now will be null and void.
	
	\section{What to Do When No Quorum Is Present}
	
	If no quorum is present and there is no hope of getting one soon, the president can call the meeting to order to satisfy the bylaw requirement that the meeting is held and then announce that there is no quorum and adjourn the meeting. Or, the president can call the meeting to order, announce to the membership that there is no quorum, and entertain a motion to recess (which enables members to try to obtain a quorum), to establish the time to adjourn (which allows the membership to set another date and time to meet and is considered a legal continuation of the current meeting), to adjourn (which means that the meeting immediately ends), or to take measures to obtain a quorum.
	
	These are the only motions allowed when no quorum is present.