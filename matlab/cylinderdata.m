radius = 0.5; height = 0.25;                     % create variables for the radius
                                            % and height of the cylinder.
surface_area = 2*pi*radius*(radius+height)  % using the formulas of the
                                            % cylinder, the surface area
                                            % and volume of the cylinder
vol = pi*(radius^2)*height                  % is calculated