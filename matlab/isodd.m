clear;                    % clears any previous variables stored in the 
                          % workspace.
clc;                      % clears the command window.
odd = logical(mod(-1,2))  % assigns a variable "odd" for the converted
                          % output to logical of the modulus function whose
                          % divisor is 2 and the dividend is the number to
                          % be evaluated.