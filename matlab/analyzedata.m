function analyzedata(filename,name)
%--------------------------------------------------------------------------
%       USAGE: analyzedata(filename,name)
%
%      AUTHOR: Je Sian Keith Ll. Herman     DATE:   Mar 28, 2020
%    MODIFIED: Je Sian Keith Ll. Herman     DATE:   Mar 29, 2020
%   
% DESCRIPTION: This function plots data from a table provided by the in the
%              format of the life_data.csv file of Mr. Herman.
%   
%      INPUTS: filename = 'filename of your file with the data' 
%              name     = 'Title of figure'
%
%     OUTPUTS: A figure with the title from name
%
%  REFERENCES: None
%--------------------------------------------------------------------------

clc;
data = readtable(filename);
    data = data(:,2:6);
    data = table2timetable(data);
    
    positiveH = data.happiness >= 0;
    negativeH = data.happiness < 0;
    positiveP = data.productivity >= 0;
    negativeP = data.productivity < 0;
    
    sumh = cumsum(data.happiness);
    sump = cumsum(data.productivity);
    
figure;
title(name);

subplot(2,1,1);
    %yyaxis left;
        bar(data.date(positiveH),...
            data.happiness(positiveH),'g','BarWidth',1);
        grid on;
        hold on;
        bar(data.date(negativeH),...
            data.happiness(negativeH),'r','BarWidth',1);
        plot(data.date,data.happiness);
    xlabel('Date');
    ylabel('Happiness');
    yticks([-9,-7,-5,-3,-1,0,1,3,5,7,9])
    
    %yyaxis right;
        %ylabel('Cumulative Happiness')
        %plot(data.date,sumh,'-o');
        hold off;
    
subplot(2,1,2);
    %yyaxis left;
        bar(data.date(positiveP),...
            data.productivity(positiveP),'g','BarWidth',1);
        hold on;
        bar(data.date(negativeP),...
            data.productivity(negativeP),'r','BarWidth',1);
        plot(data.date,data.productivity);
    xlabel('Date');
    ylabel('Productivity');
    yticks([-9,-7,-5,-3,-1,0,1,3,5,7,9]);
    
    %yyaxis right;
        %ylabel('Cumulative Productivity');
        %plot(data.date,sump,'-x');
        hold off;

figure;
title(name);        
        
subplot(1,2,1);

        plot(data.date,sumh,'-o');
        xlabel('Date');
        ylabel('Cumulative Happiness');
        grid on;
        axis square;
        hold on;
    
        plot(data.date,sump,'-x');
        legend('Cumulative Happiness','Cumulative Productivity')
        hold off;
        
subplot(1,2,2)
        yyaxis left
        plot(data.date,sumh,'-o');
        xlabel('Date');
        ylabel('Cumulative Happiness & Productivity');
        grid on;
        axis square;
        hold on;
        plot(data.date,sump,'-x');
        
        yyaxis right;
        plot(data.date,data.wake,'-s')
        plot(data.date,data.sleep,'-p')
        ylabel('Wake and Sleep Times');
        yticks(0:200:2600);
        
        legend('Cumulative Happiness',...
               'Cumulative Productivity','Wake','Sleep')
        hold off;
        
end